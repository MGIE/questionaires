-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: superCream2014
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_answer`
--

DROP TABLE IF EXISTS `tbl_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_answer` (
  `a_id` int(11) NOT NULL AUTO_INCREMENT,
  `p1_1_1` varchar(45) DEFAULT NULL,
  `p1_1_2` varchar(45) DEFAULT NULL,
  `p1_1_3` varchar(45) DEFAULT NULL,
  `p1_1_4` varchar(45) DEFAULT NULL,
  `p1_1_5` varchar(45) DEFAULT NULL,
  `p1_1_6` varchar(45) DEFAULT NULL,
  `p1_1_7` varchar(45) DEFAULT NULL,
  `p1_1_8` varchar(45) DEFAULT NULL,
  `p1_1_9` varchar(45) DEFAULT NULL,
  `p1_1_10` varchar(45) DEFAULT NULL,
  `p1_1_11` varchar(45) DEFAULT NULL,
  `p1_1_12` varchar(45) DEFAULT NULL,
  `p1_1_13` varchar(45) DEFAULT NULL,
  `p1_1_14` varchar(45) DEFAULT NULL,
  `p1_1_14_extra` varchar(255) DEFAULT NULL,
  `p1_2_1` varchar(45) DEFAULT NULL,
  `p1_2_2` varchar(45) DEFAULT NULL,
  `p1_2_3` varchar(45) DEFAULT NULL,
  `p1_2_4` varchar(45) DEFAULT NULL,
  `p1_2_5` varchar(45) DEFAULT NULL,
  `p1_2_6` varchar(45) DEFAULT NULL,
  `p1_2_7` varchar(45) DEFAULT NULL,
  `p1_2_8` varchar(45) DEFAULT NULL,
  `p1_2_9` varchar(45) DEFAULT NULL,
  `p1_2_10` varchar(45) DEFAULT NULL,
  `p1_2_10_extra` varchar(255) DEFAULT NULL,
  `p1_3_1` varchar(45) DEFAULT NULL,
  `p1_3_2` varchar(45) DEFAULT NULL,
  `p1_3_3` text,
  `p1_4` varchar(45) DEFAULT NULL,
  `p1_4_r` varchar(255) DEFAULT NULL,
  `p1_5` varchar(45) DEFAULT NULL,
  `p1_5_r` varchar(255) DEFAULT NULL,
  `p1_6` varchar(255) DEFAULT NULL,
  `p1_7_1` varchar(45) DEFAULT NULL,
  `p1_8_1_1` varchar(45) DEFAULT NULL,
  `p1_8_1_2` varchar(45) DEFAULT NULL,
  `p1_8_1_3` varchar(45) DEFAULT NULL,
  `p1_8_1_4` varchar(45) DEFAULT NULL,
  `p1_8_1_5` varchar(45) DEFAULT NULL,
  `p1_8_1_6` varchar(45) DEFAULT NULL,
  `p1_8_1_6_extra` varchar(255) DEFAULT NULL,
  `p1_8_2` varchar(45) DEFAULT NULL,
  `p1_8_2_r_1` varchar(45) DEFAULT NULL,
  `p1_8_2_r_2` varchar(45) DEFAULT NULL,
  `p1_8_2_r_3` varchar(45) DEFAULT NULL,
  `p1_8_2_r_4` varchar(45) DEFAULT NULL,
  `p1_8_2_r_5` varchar(45) DEFAULT NULL,
  `p1_8_2_r_5_extra` varchar(255) DEFAULT NULL,
  `p1_8_3` varchar(45) DEFAULT NULL,
  `p1_8_3_r` varchar(255) DEFAULT NULL,
  `p1_9_1` varchar(45) DEFAULT NULL,
  `p1_9_2` varchar(45) DEFAULT NULL,
  `p1_9_3` varchar(45) DEFAULT NULL,
  `p1_9_4` varchar(45) DEFAULT NULL,
  `p1_9_5` varchar(45) DEFAULT NULL,
  `p1_10_1_1` varchar(45) DEFAULT NULL,
  `p1_10_1_2` varchar(45) DEFAULT NULL,
  `p1_10_1_3` varchar(45) DEFAULT NULL,
  `p1_10_1_4` varchar(45) DEFAULT NULL,
  `p1_10_1_5` varchar(45) DEFAULT NULL,
  `p1_10_1_6` varchar(45) DEFAULT NULL,
  `p1_10_1_6_extra` varchar(255) DEFAULT NULL,
  `p1_10_2_1` varchar(45) DEFAULT NULL,
  `p1_10_2_2` varchar(45) DEFAULT NULL,
  `p1_10_2_3` varchar(45) DEFAULT NULL,
  `p1_10_2_4` varchar(45) DEFAULT NULL,
  `p1_10_2_5` varchar(45) DEFAULT NULL,
  `p1_10_2_6` varchar(45) DEFAULT NULL,
  `p1_10_2_6_extra` varchar(255) DEFAULT NULL,
  `p1_11` varchar(45) DEFAULT NULL,
  `p1_12_1` varchar(45) DEFAULT NULL,
  `p1_12_2` varchar(45) DEFAULT NULL,
  `p1_12_2_r` varchar(255) DEFAULT NULL,
  `p1_12_3` varchar(45) DEFAULT NULL,
  `p1_12_4` varchar(45) DEFAULT NULL,
  `p1_12_5` varchar(45) DEFAULT NULL,
  `p1_13` varchar(45) DEFAULT NULL,
  `p1_14_1` varchar(45) DEFAULT NULL,
  `p1_14_2_1` varchar(255) DEFAULT NULL,
  `p1_14_2_2` varchar(255) DEFAULT NULL,
  `p1_14_2_3` varchar(255) DEFAULT NULL,
  `p1_14_3` text,
  `p1_15_1` varchar(45) DEFAULT NULL,
  `p1_15_2_1` text,
  `p1_15_2_2` text,
  `p1_16` varchar(45) DEFAULT NULL,
  `p1_17_1` varchar(45) DEFAULT NULL,
  `p1_17_2` text,
  `p2_18_1` varchar(45) DEFAULT NULL,
  `p2_18_2` varchar(45) DEFAULT NULL,
  `p2_18_3` varchar(45) DEFAULT NULL,
  `p2_18_4` varchar(45) DEFAULT NULL,
  `p2_18_5` varchar(45) DEFAULT NULL,
  `p2_18_6` varchar(45) DEFAULT NULL,
  `p2_18_7` varchar(45) DEFAULT NULL,
  `p2_18_7_extra` varchar(255) DEFAULT NULL,
  `p2_19_1` varchar(45) DEFAULT NULL,
  `p2_19_2` varchar(45) DEFAULT NULL,
  `p2_19_3` varchar(45) DEFAULT NULL,
  `p2_19_4` varchar(45) DEFAULT NULL,
  `p2_19_5` varchar(45) DEFAULT NULL,
  `p2_19_6` varchar(45) DEFAULT NULL,
  `p2_19_7` varchar(45) DEFAULT NULL,
  `p2_19_7_extra` varchar(255) DEFAULT NULL,
  `p2_20` varchar(45) DEFAULT NULL,
  `p2_21` varchar(45) DEFAULT NULL,
  `p2_22` varchar(45) DEFAULT NULL,
  `p2_23` varchar(255) DEFAULT NULL,
  `p2_24` varchar(45) DEFAULT NULL,
  `p2_25_1` varchar(45) DEFAULT NULL,
  `p2_25_2` varchar(45) DEFAULT NULL,
  `p2_25_2_r` varchar(255) DEFAULT NULL,
  `p2_26` varchar(45) DEFAULT NULL,
  `p2_27` varchar(45) DEFAULT NULL,
  `p2_28` varchar(45) DEFAULT NULL,
  `p2_29` varchar(45) DEFAULT NULL,
  `p2_29_r` varchar(255) DEFAULT NULL,
  `p2_30` varchar(45) DEFAULT NULL,
  `p2_31` text,
  `p3_1` varchar(45) DEFAULT NULL,
  `p3_2` varchar(45) DEFAULT NULL,
  `p3_3` varchar(45) DEFAULT NULL,
  `p3_4` varchar(45) DEFAULT NULL,
  `p3_5` varchar(45) DEFAULT NULL,
  `p3_5_r` text,
  `p3_6_1` varchar(45) DEFAULT NULL,
  `p3_6_2` varchar(45) DEFAULT NULL,
  `p3_6_3` varchar(45) DEFAULT NULL,
  `p3_6_4` varchar(45) DEFAULT NULL,
  `p3_6_5` varchar(45) DEFAULT NULL,
  `p3_6_6` varchar(45) DEFAULT NULL,
  `p3_6_7` varchar(45) DEFAULT NULL,
  `p3_6_8` varchar(45) DEFAULT NULL,
  `p3_6_9` varchar(45) DEFAULT NULL,
  `p3_6_10` varchar(45) DEFAULT NULL,
  `p3_6_11` varchar(45) DEFAULT NULL,
  `p3_6_12` varchar(45) DEFAULT NULL,
  `p3_6_13` varchar(45) DEFAULT NULL,
  `p3_6_14` varchar(45) DEFAULT NULL,
  `p3_6_15` varchar(45) DEFAULT NULL,
  `p3_6_16` varchar(45) DEFAULT NULL,
  `p3_6_17` varchar(45) DEFAULT NULL,
  `p3_6_18` varchar(45) DEFAULT NULL,
  `p3_6_19` varchar(45) DEFAULT NULL,
  `p3_6_20` varchar(45) DEFAULT NULL,
  `p3_6_21` varchar(45) DEFAULT NULL,
  `p3_6_22` varchar(45) DEFAULT NULL,
  `p3_6_23` varchar(45) DEFAULT NULL,
  `p3_6_24` varchar(45) DEFAULT NULL,
  `p3_6_25` varchar(45) DEFAULT NULL,
  `p3_6_26` varchar(45) DEFAULT NULL,
  `p3_6_26_r` text,
  `p3_6_27` varchar(45) DEFAULT NULL,
  `p3_6_28` varchar(45) DEFAULT NULL,
  `p3_6_29` varchar(45) DEFAULT NULL,
  `p3_6_30` varchar(45) DEFAULT NULL,
  `p3_6_31` varchar(45) DEFAULT NULL,
  `p3_6_32` varchar(45) DEFAULT NULL,
  `p3_6_33` varchar(45) DEFAULT NULL,
  `p3_6_34` varchar(45) DEFAULT NULL,
  `p3_6_35` varchar(45) DEFAULT NULL,
  `p3_6_36` varchar(45) DEFAULT NULL,
  `p3_6_37` varchar(45) DEFAULT NULL,
  `p3_6_38` varchar(45) DEFAULT NULL,
  `p3_6_38_r` text,
  `p3_6_39` varchar(45) DEFAULT NULL,
  `p3_6_39_r` text,
  `p3_6_40` varchar(45) DEFAULT NULL,
  `p3_6_40_r` text,
  `p3_7` varchar(45) DEFAULT NULL,
  `p3_7_r` varchar(255) DEFAULT NULL,
  `p3_8` varchar(45) DEFAULT NULL,
  `p3_8_r` varchar(255) DEFAULT NULL,
  `p3_9_1` varchar(45) DEFAULT NULL,
  `p3_9_2` varchar(45) DEFAULT NULL,
  `p3_9_3` varchar(45) DEFAULT NULL,
  `p3_9_4` varchar(45) DEFAULT NULL,
  `p3_9_5` varchar(45) DEFAULT NULL,
  `p3_9_6` varchar(45) DEFAULT NULL,
  `p3_9_6_extra` varchar(255) DEFAULT NULL,
  `p3_10` varchar(45) DEFAULT NULL,
  `p3_10_r` varchar(255) DEFAULT NULL,
  `p3_11_1` varchar(45) DEFAULT NULL,
  `p3_11_2` varchar(45) DEFAULT NULL,
  `p3_11_3` varchar(45) DEFAULT NULL,
  `p3_11_4` varchar(45) DEFAULT NULL,
  `custid` varchar(255) DEFAULT NULL,
  `engname` varchar(255) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `lang` text,
  `createTime` datetime DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  `ip` text,
  `userAgent` text,
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_question`
--
DROP TABLE IF EXISTS `tbl_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_question` (
  `q_id` int(11) NOT NULL AUTO_INCREMENT,
  `q_formid` varchar(45) DEFAULT '',
  `q_displayid` varchar(45) DEFAULT NULL,
  `q_section` int(11) DEFAULT NULL,
  `q_type` varchar(45) DEFAULT NULL,
  `q_order` int(4) DEFAULT NULL,
  `q_text_en` text,
  `q_text_tc` text,
  `q_text_sc` text,
  `q_text_th` text,
  `q_text_kr` text,
  `q_text_jp` text,
  `q_class` varchar(45) DEFAULT NULL,
  `q_data` varchar(255) DEFAULT NULL,
  `q_meta` text,
  `q_submit_check` varchar(45) DEFAULT 'required',
  PRIMARY KEY (`q_id`)
) ENGINE=InnoDB AUTO_INCREMENT=392 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `tbl_question` WRITE;
/*!40000 ALTER TABLE `tbl_question` DISABLE KEYS */;
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (100,'p1_1',NULL,1,'NEW_CHECKBOX',1000,'','Q1.	您是如何得知<b>Ultra Facial Cream Intense Hydration深層滋潤保濕乳霜</b>',NULL,NULL,NULL,NULL,NULL,NULL,'透過報紙、雜誌或傳媒介紹\n透過網上傳媒介紹\n透過網上試用文章或討論區介紹\n櫥窗設計\n店內擺設\n朋友介紹\nKiehl’s發出的宣傳郵件\n信用卡發出的宣傳郵件\nKiehl’s Facebook Fanpage\nKiehl’s iPhone/Android App\nKiehl’s顧客服務代表主動介紹\n在店內試用產品\n在店內取得試用裝\n其他 (請列出)：\n[OTHERS]','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (291,'',NULL,0,'SECTION',NULL,NULL,'<div style=\"margin-top:-20px;width:100%;text-align:center;display:block;\"><img src=\"images/product2.png\" width=\"150\"></div>\n親愛的顧客：多謝您對<b>Kiehl’s深層滋潤保濕乳霜</b>的興趣和支持，並感謝您抽空回答這份問卷。為答謝您對Kiehl’s的支持，您只需於<b>2014年8月6日前</b>成功遞交此問卷，經專人核實問卷資料完整，我們將於<b>2014年8月13日或之前</b>透過SMS為您送上<b>Kiehl’s醫學維C淨白保濕面膜 (1pc)、 醫學革新維C淡斑精華(4ml) 以及身體潤膚乳(65ml)</b>的換領短訊。\n<br>\n<br>\n請用滑鼠點選項目\n<div class=\"listArea\">\n<div class=\"list_item\">\n						顧客編號：\n						</div>\n						<div class=\"list_item\">\n							45<input name=\"custid\" id=\"custid\" type=\"text\" maxlength=\"9\" style=\"width: 135px; font-size: 16px; font-family: Times; border: 0px; border-bottom: 1px black dashed; padding: 1px 0px 0px 0px; margin: 0px\"><br>\n							(可參考列印於信封上或於電郵內的顧客編號)\n							<div id=\"custidError\" style=\"display: none; color: red;\">\n								請輸入編號.\n							</div>\n							</div>\n							</div>\n				<div class=\"listArea\">\n						<div class=\"list_item\">\n							顧客英文姓名：\n							</div>\n							<div class=\"list_item\">\n							<input name=\"engname\" id=\"engname\" type=\"text\" maxlength=\"50\">			\n							<div id=\"engnameError\" style=\"display: none; color: red;\">\n								請輸入英文名字.\n							</div>\n							</div>\n							</div>\n<div class=\"listArea\">\n						<div class=\"list_item\">\n\n手提電話：</div>\n<div class=\"list_item\">\n						<input name=\"mobile\" id=\"mobile\" type=\"text\" maxlength=\"8\">				\n							<div id=\"phoneError\" style=\"display: none; color: red;\">\n								請輸入手提電話.\n							</div>\n							</div>\n							</div>\n					<div class=\"listArea\">\n						<div class=\"list_item\">\n							換領禮品地點： </div>\n							<div class=\"list_item\">\n							<div id=\"locationError\" style=\"display: none; color: red;\">\n								請選擇換領禮品地點.\n							</div>\n							<input id=\"location\" name=\"location\" type=\"radio\" value=\"1\">\n							銅鑼灣軒尼詩道專門店\n							<br>\n							<input name=\"location\" type=\"radio\" value=\"2\">\n							太古廣場 Harvey Nichols 專櫃 \n							<br>\n							<input name=\"location\" type=\"radio\" value=\"3\">\n							金鐘廊LAB CONCEPT 專櫃\n							<br>\n							<input name=\"location\" type=\"radio\" value=\"4\">\n							圓方專門店\n							<br>\n							<input name=\"location\" type=\"radio\" value=\"5\">\n							將軍澳 PopCorn 專門店\n							<br>\n							<input name=\"location\" type=\"radio\" value=\"6\">\n							屯門市廣場專門店\n							<br>\n							<input name=\"location\" type=\"radio\" value=\"7\">\n							葵芳新都會廣場專門店\n							<br>\n							<input name=\"location\" type=\"radio\" value=\"8\">\n							澳門新八佰伴專門店	\n							<br>							\n						</div>\n						</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (308,'p1_2',NULL,1,'NEW_CHECKBOX',900,NULL,'Q2.當您購買<b>深層滋潤保濕乳霜</b>時，<u>最先考慮的三項條件</u>是甚麼？（請選出3 項）',NULL,NULL,NULL,NULL,NULL,NULL,'價錢 (<b>深層滋潤保濕乳霜</b>$305/50ml)\n品牌\n產品質地\n功效\n方便使用\n朋友的意見\n傳媒介紹\n產品包裝\n網上試用文章或討論區介紹\n新推出產品\n其他 (請列出)：\n[OTHERS]','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (310,'p1_3_1',NULL,1,'SELECT',800,NULL,'Q3.(a)您給予<b>深層滋潤保濕乳霜</b>櫥窗設計及店內擺設的評分是：<br>\n<div class=\"listImgWrap\">\n<div class=\"listImg\">\n<img src=\"images/img4.png\">&nbsp;&nbsp;\n</div>\n<div class=\"listImg\">\n<img src=\"images/img5.png\">&nbsp;&nbsp;\n</div>\n<div class=\"listImg\">\n<img src=\"images/img6.png\">\n</div>\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (311,'p1_3_2',NULL,1,'SELECT',700,NULL,'(b)您認為櫥窗設計能否令您感覺得此產品是保濕產品?\r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (312,'p1_3_3',NULL,1,'TEXTAREA',600,NULL,'其他關於櫥窗設計及店內擺設的意見：',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (313,'p1_4',NULL,1,'SELECT',500,NULL,'Q4.您購買<b>深層滋潤保濕乳霜</b>是為了…？',NULL,NULL,NULL,NULL,'equal2','show|p1_4_r*p1_4_t',NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (315,'p1_5',NULL,1,'SELECT',450,NULL,'Q5.您有沒有使用過所購買的<b>深層滋潤保濕乳霜</b>？',NULL,NULL,NULL,NULL,'equal2','show|p1_5_r*p1_5_t',NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (316,'p1_5_r',NULL,1,'INPUT_TEXT',440,NULL,'原因：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (317,'p1_4_r',NULL,1,'INPUT_TEXT',490,NULL,'送給：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (319,'p1_6',NULL,1,'INPUT_TEXT',430,NULL,'Q6.請問您使用了<b>深層滋潤保濕乳霜多久</b>？',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (320,'',NULL,1,'TEXT',420,NULL,'Q7.您使用產品的次數是：',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (321,'p1_7_1',NULL,1,'SELECT',410,NULL,'<b>深層滋潤保濕乳霜</b>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (322,'p1_8_1',NULL,1,'NEW_CHECKBOX',400,NULL,'Q8. 您會配合<b>深層滋潤保濕乳霜</b>一同使用的護膚產品包括：',NULL,NULL,NULL,NULL,NULL,NULL,'潔面乳\n爽膚水\n眼霜\n精華素\n防曬霜\n其他\n[OTHERS]','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (326,'p1_9',NULL,1,'SELECT',NULL,NULL,'Q9. 有否同時使用其他品牌的保濕產品？',NULL,NULL,NULL,NULL,'equal1','show|p1_9_r',NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (327,'p1_9_r',NULL,1,'INPUT_TEXT',NULL,NULL,'請列出品牌及產品名稱：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (329,'p1_10_1',NULL,1,'NEW_CHECKBOX',NULL,NULL,'Q10.  (a) 您喜歡<b>深層滋潤保濕乳霜</b>的以下哪些<b><u>產品功效</u></b>？（可選多項）',NULL,NULL,NULL,NULL,NULL,NULL,'改善脫皮\n紓緩乾裂\n撫平乾紋\n24小時保濕效能\n其他（請列出）：\n[OTHERS]','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (330,'p1_10_2',NULL,1,'NEW_CHECKBOX',NULL,NULL,'(b) 承上題，哪一項功效最顯著呢？',NULL,NULL,NULL,NULL,NULL,NULL,'改善脫皮\n紓緩乾裂\n撫平乾紋\n24小時保濕效能\n其他（請列出）：\n[OTHERS]','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (331,'p1_11',NULL,1,'NEW_SELECT',NULL,NULL,'Q11. 您認為<b>深層滋潤保濕乳霜</b>的功效怎樣？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]十分有效\n2 ||[tc]有效\n3 ||[tc]一般\n4 ||[tc]沒有效用\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (332,'p1_12',NULL,1,'TEXT',NULL,NULL,'Q12. 關於<b><u>產品質感</u></b>方面，您會如何形容您所選購的<b>深層滋潤保濕乳霜</b>？',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (333,'p1_12_1',NULL,1,'NEW_SELECT',NULL,NULL,'(a) 您覺得這款產品的質感怎樣？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]太豐潤\n2 ||[tc]適中\n3 ||[tc]太薄\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (334,'p1_12_2',NULL,1,'SELECT',NULL,NULL,'(b) 肌膚用後感覺如何？',NULL,NULL,NULL,NULL,'equal4','show|p1_12_2_r',NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (335,'p1_12_2_r',NULL,1,'INPUT_TEXT',NULL,NULL,'請填寫',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (336,'p1_12_3',NULL,1,'NEW_SELECT',NULL,NULL,'(c) 產品是否容易吸收？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]迅速吸收\n2 ||[tc]普通\n3 ||[tc]難吸收\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (337,'p1_12_4',NULL,1,'NEW_SELECT',NULL,NULL,'(d) 產品是否容易塗抹？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n 1 ||[tc]很難塗抹\n 2 ||[tc]容易推開\n ','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (338,'p1_12_5',NULL,1,'NEW_SELECT',NULL,NULL,'(e) 您是否喜歡產品的氣味？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]是\n2 ||[tc]否\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (339,'p1_13',NULL,1,'NEW_SELECT',NULL,NULL,'Q13. 您認為深層滋潤保濕乳霜的售價怎樣？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]昂貴\n2 ||[tc]適中\n3 ||[tc]相宜\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (340,'p1_14_1',NULL,1,'NEW_SELECT',NULL,NULL,'Q.14(a) Kiehl’s的<b>深層滋潤保濕乳霜</b>有沒有取代您之前使用的護膚產品？',NULL,NULL,NULL,NULL,'equal2','show|p1_14_1_r','-1||[tc]-- 請選擇 --\n1 ||[tc]有\n2 ||[tc]沒有\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (341,'p1_14_1_r',NULL,1,'TEXT',NULL,NULL,'請跳答Q15',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (342,'p1_14_2',NULL,1,'TEXT',NULL,NULL,'(b) 如有，請列出品牌、產品名稱及產品性質：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (343,'p1_14_2_1',NULL,1,'INPUT_TEXT',NULL,NULL,'品牌： 	',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (344,'p1_14_2_2',NULL,1,'INPUT_TEXT',NULL,NULL,'產品名稱：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (345,'p1_14_2_3',NULL,1,'INPUT_TEXT',NULL,NULL,'產品性質：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (346,'p1_14_3',NULL,1,'TEXTAREA',NULL,NULL,'(c) 為何會轉用Kiehl’s深層滋潤保濕乳霜？',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (347,'p1_15_1',NULL,1,'NEW_SELECT',NULL,NULL,'Q.15 (a) 總括來說，您認為<b>深層滋潤保濕乳霜</b>比起之前使用的其他品牌產品較…？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]差很多\n2 ||[tc]差\n3 ||[tc]一樣\n4 ||[tc]優勝\n5 ||[tc]優勝很多\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (348,'p1_15_2_1',NULL,1,'TEXTAREA',NULL,NULL,'(b) 哪些方面比較差?',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (349,'p1_15_2_2',NULL,1,'TEXTAREA',NULL,NULL,'哪些方面比較優勝?',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (350,'p1_16',NULL,1,'NEW_SELECT',NULL,NULL,'Q.16 您對<b>深層滋潤保濕乳霜</b>的整體滿意程度是：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]非常滿意\n2 ||[tc]滿意\n3 ||[tc]不滿意\n4 ||[tc]非常不滿意\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (351,'p1_17_1',NULL,1,'NEW_SELECT',NULL,NULL,'Q.17 (a) 最後，您會否再次購買<b>深層滋潤保濕乳霜</b>？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]是\n2 ||[tc]否\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (352,'p1_17_2',NULL,1,'TEXTAREA',NULL,NULL,'您的原因是：',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (353,'p2_18',NULL,2,'NEW_CHECKBOX',NULL,NULL,'Q.18   您通常如何獲得護膚資訊？（可選多項）',NULL,NULL,NULL,NULL,NULL,NULL,'大眾傳媒\n互聯網 (Beauty Blog, Forum, Website)\n店內擺設\n產品小冊子\n朋友介紹\n店員介紹\n其他（請列出）：\n[OTHERS]','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (354,'p2_19',NULL,2,'NEW_CHECKBOX',NULL,NULL,'Q.19甚麼原因會促使您試用新的護膚產品？（可選多項） ',NULL,NULL,NULL,NULL,NULL,NULL,'不滿現有產品效果\n護膚需要有所改變\n新產品功效吸引\n朋友介紹\n店員介紹\n傳媒/互聯網推介\n其他（請列出）：\n[OTHERS]','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (355,'p2_20',NULL,2,'NEW_SELECT',NULL,NULL,'Q.20  您平均多久會購買一次護膚產品？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]每星期一次\n2 ||[tc]每兩至三星期一次\n3 ||[tc]每月一次\n4 ||[tc]每兩至三月一次\n5 ||[tc]四個月以上一次','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (356,'p2_21',NULL,2,'NEW_SELECT',NULL,NULL,'Q.21 您在過去三個月花費多少於護膚產品上？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1||[tc] $300以下\n2||[tc] $301-$1,000\n3||[tc] $1,001-$2,000\n4||[tc] $2,001 以上','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (357,'p2_22',NULL,2,'NEW_SELECT',NULL,NULL,'Q.22 您是否第一次購買Kiehl’s產品？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]是\n2 ||[tc]否\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (358,'p2_23',NULL,2,'NEW_SELECT',NULL,NULL,'Q.23   最常前往購物的Kiehl’s專門店：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1||[tc]置地廣場\n2||荃灣綠楊坊\n3||時代廣場連卡佛\n4||apm \n5||銅鑼灣羅素街地下\n6||新城市廣場4樓\n7||海港城海運大廈FACES\n8||葵芳新都會廣埸\n9||海港城港威商場\n10||尖沙咀The ONE Beauty Bazaar \n11||朗豪坊2樓\n12||屯門市廣埸\n13||圓方\n14||九龍灣德福廣場1期\n15||又一城\n16||銅鑼灣軒尼詩道地下\n17||將軍澳PopCorn\n18||上水廣場2樓\n19||金鐘太古廣場 Harvey Nichols\n20||金鐘廊Faces at LAB\n21||ifc中環國際金融中心\n22||澳門八佰伴1樓','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (359,'p2_24',NULL,2,'NEW_SELECT',NULL,NULL,'Q.24 Kiehl’s舖頭內的貨架是否清晰，讓您容易找到心儀產品？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]是\n2 ||[tc]否\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (360,'p2_25_1',NULL,2,'NEW_SELECT',NULL,NULL,'Q.25 (a) 在您上一次購物，您有否收到Kiehl’s的試用裝：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]是\n2 ||[tc]否\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (361,'p2_25_2',NULL,2,'NEW_SELECT',NULL,NULL,'Q.25 (b) 如有收到，您有沒有使用?',NULL,NULL,NULL,NULL,'equal2','show|p2_25_2_r','-1||[tc]-- 請選擇 --\n1 ||[tc]有\n2 ||[tc]沒有\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (362,'p2_25_2_r',NULL,2,'INPUT_TEXT',NULL,NULL,'為什麼?',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (363,'p2_26',NULL,2,'NEW_SELECT',NULL,NULL,'Q.26 您對Kiehl’s顧客服務代表的<b><u>服務質素</u></b>評分是：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]極差\n2 ||[tc]差\n3 ||[tc]標準\n4 ||[tc]好\n5 ||[tc]極好','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (364,'p2_27',NULL,2,'NEW_SELECT',NULL,NULL,'Q.27   您對Kiehl’s顧客服務代表的<b><u>產品知識</u></b>評分是：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]極差\n2 ||[tc]差\n3 ||[tc]標準\n4 ||[tc]好\n5 ||[tc]極好','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (365,'p2_28',NULL,2,'NEW_SELECT',NULL,NULL,'Q.28   您給Kiehl’s顧客服務代表在<b><u>皮膚諮詢過程中的專業程度</u></b>評分是：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]極差\n2 ||[tc]差\n3 ||[tc]標準\n4 ||[tc]好\n5 ||[tc]極好','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (366,'p2_29',NULL,2,'NEW_SELECT',NULL,NULL,'Q.29   您覺得現時Kiehl’s專門店的店舖位置是否方便？\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n置地廣場\n</div>\n<div class=\"list_shopName_item\">\n荃灣綠楊坊\n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n時代廣場連卡佛\n</div>\n<div class=\"list_shopName_item\">\napm \n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n銅鑼灣羅素街地下\n</div>\n<div class=\"list_shopName_item\">\n新城市廣場4樓\n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n海港城海運大廈FACES\n</div>\n<div class=\"list_shopName_item\">\n葵芳新都會廣埸\n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n海港城港威商場\n</div>\n<div class=\"list_shopName_item\">\n尖沙咀The ONE Beauty Bazaar \n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n朗豪坊2樓\n</div>\n<div class=\"list_shopName_item\">\n屯門市廣埸\n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n圓方\n</div>\n<div class=\"list_shopName_item\">\n九龍灣德福廣場1期\n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n又一城\n</div>\n<div class=\"list_shopName_item\">\n銅鑼灣軒尼詩道地下\n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n將軍澳PopCorn\n</div>\n<div class=\"list_shopName_item\">\n上水廣場2樓\n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\n金鐘太古廣場 Harvey Nichols\n</div>\n<div class=\"list_shopName_item\">\n金鐘廊Faces at LAB\n</div>\n</div>\n<div class=\"listNameArea\">\n<div class=\"list_shopName_item\">\nifc中環國際金融中心\n</div>\n<div class=\"list_shopName_item\">\n澳門八佰伴1樓\n</div>\n</div>',NULL,NULL,NULL,NULL,'equal2','show|p2_29_r','-1||[tc]-- 請選擇 --\n1 ||[tc]是\n2 ||[tc]不是\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (367,'p2_29_r',NULL,2,'INPUT_TEXT',NULL,NULL,'您會建議哪些地方？',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (368,'p2_30',NULL,2,'NEW_SELECT',NULL,NULL,'Q30 總括來說，您認為Kiehl’s的產品售價怎樣？',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]很昂貴\n2 ||[tc]略高\n3 ||[tc]適中\n4 ||[tc]適中\n','required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (369,'p2_31',NULL,2,'TEXTAREA',NULL,NULL,'Q31 有沒有其他有關Kiehl’s回應或意見？',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (370,'',NULL,3,'TEXT',NULL,NULL,'<b><u>個人資料（請盡量填寫）</u></b>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (371,'p3_1',NULL,3,'NEW_SELECT',NULL,NULL,'S1-年齡：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]19 歲或以下\n2 ||[tc]20 – 24 歲\n3 ||[tc]25 – 29歲\n4 ||[tc]30 – 34歲\n5 ||[tc]35 – 39歲\n6 ||[tc]40 – 44歲\n7 ||[tc]45 – 49歲\n8 ||[tc]50歲或以上\n','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (372,'p3_2',NULL,3,'NEW_SELECT',NULL,NULL,'S2-	職業：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]商人\n2 ||[tc]專業人士\n3 ||[tc]白領\n4 ||[tc]服務行業\n5 ||[tc]學生\n6 ||[tc]其他／暫沒工作\n','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (373,'p3_3',NULL,3,'NEW_SELECT',NULL,NULL,'S3-	個人每月收入：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]HK$10,000 或以下\n2 ||[tc]HK$10,001 – HK$20,000\n3 ||[tc]HK$20,001 – HK$30,000\n4 ||[tc]HK$30,001 或以上\n','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (374,'p3_4',NULL,3,'NEW_SELECT',NULL,NULL,'S4-	皮膚性質：',NULL,NULL,NULL,NULL,NULL,NULL,'-1||[tc]-- 請選擇 --\n1 ||[tc]極油性\n2 ||[tc]油性\n3 ||[tc]中性\n4 ||[tc]乾性\n4 ||[tc]極乾性\n4 ||[tc]混合性\n4 ||[tc]敏感性\n','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (375,'p3_5',NULL,3,'SELECT',NULL,NULL,'S5-	您最常瀏覽哪些網頁?',NULL,NULL,NULL,NULL,'equal17','show|p3_5_r',NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (376,'p3_5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (377,'p3_5_r',NULL,3,'INPUT_TEXT',NULL,NULL,'請列出：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (378,'p3_6',NULL,3,'CHECKBOX',NULL,NULL,'S6- 您通常看哪些報紙／雜誌？（可選多項） ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (379,'p3_7',NULL,3,'NEW_SELECT',NULL,NULL,'S7-	您有否瀏覽過Kiehl’s的官方網頁?',NULL,NULL,NULL,NULL,'equal2','show|p3_7_r','-1||[tc]-- 請選擇 --\n1 ||[tc]是\n2 ||[tc]不是','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (380,'p3_7_r',NULL,3,'INPUT_TEXT',NULL,NULL,'原因：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (381,'p3_8',NULL,3,'NEW_SELECT',NULL,NULL,'S8-	您是否Kiehl’s的Facebook Fans?',NULL,NULL,NULL,NULL,'equal2','show|p3_8_r','-1||[tc]-- 請選擇 --\n1 ||[tc]是\n2 ||[tc]不是','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (382,'p3_8_r',NULL,3,'INPUT_TEXT',NULL,NULL,'原因：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (383,'p3_9',NULL,3,'NEW_CHECKBOX',NULL,NULL,'S9-	除Facebook外，您有否使用其他的電子社交網絡平台?',NULL,NULL,NULL,NULL,NULL,NULL,'微博 (Weibo)\nTwitter\nInstagram\n微信 (Wechat)\n沒有\n其他\n[OTHERS]','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (384,'p3_10',NULL,3,'NEW_SELECT',NULL,NULL,'S10-	您有否下載Kiehl’s的iPhone / Android App?',NULL,NULL,NULL,NULL,'equal2','show|p3_10_r','-1||[tc]-- 請選擇 --\n1 ||[tc]有\n2 ||[tc]沒有\n','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (385,'p3_10_r',NULL,3,'INPUT_TEXT',NULL,NULL,'原因：',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (386,'p3_11',NULL,3,'NEW_CHECKBOX',NULL,NULL,'S11-	您有否使用以下的電子產品?',NULL,NULL,NULL,NULL,NULL,NULL,'平板電腦(如iPad)\nAndroid系統手提電話\niPhone系統手提電話\n沒有','');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (387,'',NULL,3,'TEXT',NULL,NULL,'\n<table width=\"100%\" border=\"0\"><tr><td align=\"center\">-  問卷完 –</td></tr></table>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (389,'p1_4_t',NULL,1,'TEXT',480,NULL,'<button type=\"button\" class=\"btn btn-default btn-sm btn-block\" onclick=\"kiehlSurveyController.saveQuestions()\" style=\"width: 20%;\">請跳答Q18</button>',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (390,'p1_5_t',NULL,1,'TEXT',435,NULL,'<button type=\"button\" class=\"btn btn-default btn-sm btn-block\" onclick=\"kiehlSurveyController.saveQuestions()\" style=\"width: 20%;\">請跳答Q18</button>',NULL,NULL,NULL,NULL,'qHide',NULL,NULL,'required');
INSERT INTO `tbl_question` (`q_id`, `q_formid`, `q_displayid`, `q_section`, `q_type`, `q_order`, `q_text_en`, `q_text_tc`, `q_text_sc`, `q_text_th`, `q_text_kr`, `q_text_jp`, `q_class`, `q_data`, `q_meta`, `q_submit_check`) VALUES (391,'',NULL,1,'TEXT',429,NULL,'(月/ 星期)　',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'required');
/*!40000 ALTER TABLE `tbl_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_question_option`
--

DROP TABLE IF EXISTS `tbl_question_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_question_option` (
  `o_id` int(11) NOT NULL AUTO_INCREMENT,
  `q_id` int(11) DEFAULT NULL,
  `o_displayid` varchar(45) DEFAULT NULL,
  `o_extra` varchar(45) DEFAULT NULL,
  `o_value` varchar(45) DEFAULT NULL,
  `o_text_en` text,
  `o_text_tc` text,
  `o_text_sc` text,
  `o_text_th` text,
  `o_text_kr` text,
  `o_text_jp` text,
  `hide` varchar(45) DEFAULT NULL,
  `o_priority` int(11) DEFAULT '0',
  PRIMARY KEY (`o_id`)
) ENGINE=InnoDB AUTO_INCREMENT=477 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_question_option`
--

LOCK TABLES `tbl_question_option` WRITE;
/*!40000 ALTER TABLE `tbl_question_option` DISABLE KEYS */;
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (378,310,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (379,310,NULL,NULL,'1',NULL,'非常差',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (380,310,NULL,NULL,'2',NULL,'差',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (381,310,NULL,NULL,'3',NULL,'一般',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (382,310,NULL,NULL,'4',NULL,'好',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (383,310,NULL,NULL,'5',NULL,'非常好',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (384,311,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (385,311,NULL,NULL,'1',NULL,'是',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (386,311,NULL,NULL,'2',NULL,'否',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (387,313,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (388,313,NULL,NULL,'1',NULL,'自用',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (389,313,NULL,NULL,'2',NULL,'送給別人',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (390,315,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (391,315,NULL,NULL,'1',NULL,'有',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (392,315,NULL,NULL,'2',NULL,'沒有',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (394,321,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (395,321,NULL,NULL,'1',NULL,'	每天使用一次',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (396,321,NULL,NULL,'2',NULL,'每天早晚使用',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (397,321,NULL,NULL,'3',NULL,'每星期兩至三次',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (398,321,NULL,NULL,'4',NULL,'每星期一次',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (399,321,NULL,NULL,'5',NULL,'很少使用',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (400,324,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (401,324,NULL,NULL,'1',NULL,'有',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (402,324,NULL,NULL,'2',NULL,'沒有',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (403,326,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (404,326,NULL,NULL,'1',NULL,'有',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (405,326,NULL,NULL,'2',NULL,'沒有',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (407,334,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (408,334,NULL,NULL,'1',NULL,'舒適',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (409,334,NULL,NULL,'2',NULL,'清爽',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (410,334,NULL,NULL,'3',NULL,'黏膩',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (411,334,NULL,NULL,'4',NULL,'其他',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (416,375,NULL,NULL,'-1',NULL,'--請選擇--',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (417,375,NULL,NULL,'1',NULL,'discuss.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (418,375,NULL,NULL,'2',NULL,'uwants.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (419,375,NULL,NULL,'3',NULL,'beautyexchange.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (420,375,NULL,NULL,'4',NULL,'she.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (421,375,NULL,NULL,'5',NULL,'ztyle.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (422,375,NULL,NULL,'6',NULL,'appledaily.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (423,375,NULL,NULL,'7',NULL,'esdlife.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (424,375,NULL,NULL,'8',NULL,'qooza.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (425,375,NULL,NULL,'9',NULL,'高登',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (426,375,NULL,NULL,'10',NULL,'Uwants	',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (427,375,NULL,NULL,'11',NULL,'sina.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (428,375,NULL,NULL,'12',NULL,'facebook.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (429,375,NULL,NULL,'13',NULL,'yahoo',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (430,375,NULL,NULL,'14',NULL,'cosmopolitan.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (431,375,NULL,NULL,'15',NULL,'elle.com',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (432,375,NULL,NULL,'16',NULL,'淘寶',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (433,375,NULL,NULL,'17',NULL,'其他：',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (434,378,NULL,NULL,'-1',NULL,'<b><u>報紙</u></b>',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (435,378,NULL,NULL,'1',NULL,'am730',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (436,378,NULL,NULL,'2',NULL,'蘋果日報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (437,378,NULL,NULL,'3',NULL,'頭條日報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (438,378,NULL,NULL,'4',NULL,'信報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (439,378,NULL,NULL,'5',NULL,'經濟日報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (440,378,NULL,NULL,'6',NULL,'都市日報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (441,378,NULL,NULL,'7',NULL,'東方日報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (442,378,NULL,NULL,'8',NULL,'星島日報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (443,378,NULL,NULL,'9',NULL,'南華早報South China Morning Post',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (444,378,NULL,NULL,'10',NULL,'The Standard',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (445,378,NULL,NULL,'11',NULL,'太陽報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (446,378,NULL,NULL,'12',NULL,'明報',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (447,378,NULL,NULL,'-1',NULL,'<b><u>周刊／雙周刊</u></b>',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (448,378,NULL,NULL,'13',NULL,'東Touch',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (449,378,NULL,NULL,'14',NULL,'快周刊',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (450,378,NULL,NULL,'15',NULL,'Face 便利',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (451,378,NULL,NULL,'16',NULL,'Milk',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (452,378,NULL,NULL,'17',NULL,'明報周刊',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (453,378,NULL,NULL,'18',NULL,'新Monday',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (454,378,NULL,NULL,'19',NULL,'壹周刊',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (455,378,NULL,NULL,'20',NULL,'U Magazine',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (456,378,NULL,NULL,'21',NULL,'新假期',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (457,378,NULL,NULL,'22',NULL,'忽然一周',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (458,378,NULL,NULL,'23',NULL,'東方新地',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (459,378,NULL,NULL,'24',NULL,'100毛',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (461,378,NULL,NULL,'26',NULL,'其他<br><div class=\"qHideWrap\" ><input id=\"p3_6_26_r\"   name=\"p3_6_26_r\" type=\"text\" class=\"form-control\" placeholder=\"請輸入...\"></div>',NULL,NULL,NULL,NULL,'1',0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (462,378,NULL,NULL,'-1',NULL,'<b><u>月刊</u></b>',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (463,378,NULL,NULL,'27',NULL,'CosmoGirl',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (464,378,NULL,NULL,'28',NULL,'Cosmopolitan',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (465,378,NULL,NULL,'29',NULL,'Elle',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (466,378,NULL,NULL,'30',NULL,'Elle Men',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (467,378,NULL,NULL,'31',NULL,'Harper’s Bazaar',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (468,378,NULL,NULL,'32',NULL,'Jessica Code',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (469,378,NULL,NULL,'33',NULL,'Jessica旭茉',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (470,378,NULL,NULL,'34',NULL,'Marie Claire瑪麗嘉兒',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (471,378,NULL,NULL,'35',NULL,'Men’s Uno',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (472,378,NULL,NULL,'36',NULL,'MR.',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (473,378,NULL,NULL,'37',NULL,'君子雜誌Esquire',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (474,378,NULL,NULL,'38',NULL,'西方雜誌<br><div class=\"qHideWrap\" ><input id=\"p3_6_38_r\"   name=\"p3_6_38_r\" type=\"text\" class=\"form-control\" placeholder=\"請輸入...\"></div>',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (475,378,NULL,NULL,'39',NULL,'日本雜誌<br><div class=\"qHideWrap\" ><input id=\"p3_6_39_r\"   name=\"p3_6_39_r\" type=\"text\" class=\"form-control\" placeholder=\"請輸入...\"></div>',NULL,NULL,NULL,NULL,NULL,0);
INSERT INTO `tbl_question_option` (`o_id`, `q_id`, `o_displayid`, `o_extra`, `o_value`, `o_text_en`, `o_text_tc`, `o_text_sc`, `o_text_th`, `o_text_kr`, `o_text_jp`, `hide`, `o_priority`) VALUES (476,378,NULL,NULL,'40',NULL,'其他：<br><div class=\"qHideWrap\" ><input id=\"p3_6_40_r\"   name=\"p3_6_40_r\" type=\"text\" class=\"form-control\" placeholder=\"請輸入...\"></div>',NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `tbl_question_option` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-23 15:10:21
