<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link rel="stylesheet" href="../../css/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" href="../../css/bootstrap/bootstrap-theme.min.css">
	</head>
	<body>
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#langPanel">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a id="btnDownload" class="navbar-brand" href="#download">Download</a>
					</div>
				</div>
			</nav>

			<div id="main">

			</div>
		</div>
		<script src="../../js/jquery-1.10.2.min.js"></script>
		<script src="../../js/bootstrap.min.js"></script>
		<!--[if lt IE 9]>
		<script src="../../js/html5shiv.js"></script>
		<script src="../../js/respond.min.js"></script>
		<![endif]-->
		<script>
			$(document).ready(function() {
			    
				$('#btnDownload').click(function() {
					$.get('../../api/downloadCSV.php', function(file) {
						$(window).attr('location', '../../csv/' + file);
					}, 'json');
				});
			});
		</script>
	</body>
</html>