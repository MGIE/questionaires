var kiehlSurveyLanguage = {
	getText : function(lang, alias) {
		var text = this[lang][alias];
                
		if (text == null) {
                    if(lang!='tc'){
			text = this.getText("tc", alias);
                    }
		}

		return text;
	},
	"en" : {
		"code" : "en",
		"Title" : "Ultra Facial Cream Intense Hydration<br>深層滋潤保濕乳霜<br>問卷調查",
		"Home" : "Home",
		"Next" : "Next",
		"Finish" : "Finish",
		"ThanksMessage" : "Finish. Thanks for your participation.<br>請關閉視窗"
	},
	"tc" : {
		"code" : "zh-Hant",
                "Title" : 'Ultra Facial Cream Intense Hydration<br>深層滋潤保濕乳霜<br>問卷調查',
		"Home" : "主頁",
		"Next" : "下一頁",
		"Finish" : "完成",
   	  "ThanksMessage" : '<p>再一次感謝您參與這次問卷調查，您的寶貴意見將有助Kiehl’s在將來提供更優質的產品及服務。若您在期間有任何疑問或查詢，歡迎電郵至<u><a href="mailto:kiehls@hk.loreal.com" target="_blank">kiehls@hk.loreal.com</a></u>或致電(852) 3180 1651與我們聯絡。Kiehl’s員工謹此送上至誠的祝福。我們熱切冀盼能為您效勞。 </p><p> <div style="margin-top:20px;width:100%;text-align:center;"><u>個人資料蒐集聲明</u> </div><p>在此問卷內所獲取之一切個人資料，將只會被Kiehl’s用作內部參考，方便日後為顧客提供最合適之產品和服務。請注意：基於安全理由，我們絕不會透過電話問及你的身份證號碼，而你亦切勿將之透露予致電者。Kiehl’s會採用所有可行之程序，保障你個人資料的私隱權。我們不會向任何外界機構透露於此問卷所取得之個人資料，除非已獲你同意，或按法律規定進行，或事前已通知。此外，你的個人資料只會保留一段時間。日後我們會根據內部文件保留政策，銷毀你的個人資料。 </p>'	
   	  },
	"sc" : {
		"code" : "zh-Hans",
		"Home" : "主页",
		"Next" : "下一页",
		"Finish" : "完成"
	},
	"th" : {
		"code" : "th"
	},
	"kr" : {
		"code" : "ko"
	},
	"jp" : {
		"code" : "ja",
		"Title" : "ロレアル リュクス<br>トラベル リーテイル アジア パシフィック"
	}
};
