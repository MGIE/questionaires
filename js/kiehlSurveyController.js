var crossPageValue = {};
var kiehlSurveyController = {
	device : null,
	lang : "tc",
	section : 0,
	a_id : 0,
	checking : false,
	finished : false,
	initial : function(lang, device) {
		this.lang = lang;
		this.device = device;
		$('html').attr('lang', this.getText('code'));
		$('#surveyTitle').html(this.getText('Title'));
	},
	changeLanguage : function(lang) {
		this.initial(lang);
		this.getQuestions(this.section);
	},
	getText : function(alias) {
		var langPack = kiehlSurveyLanguage;
		var text = langPack.getText(this.lang, alias);

		return text;
	},
	getQuestions : function(section) {
		this.section = section;
		var _this = this;

		if (_this.finished) {
			_this.showThanksMessage();
			return;
		}

		$.get('api/getQuestions.php', {
			"lang" : _this.lang,
			"section" : section
		}, function(questions) {
			var html = '';
			for (var i = 0; i < questions.length; i++) {
				html += _this.displayQuestion(questions[i]);
			}
			html += '<div class="col-sm-12 MG_BUTTON_PANEL"><button type="button" class="btn btn-default btn-lg btn-block" onclick="kiehlSurveyController.saveQuestions()">' + _this.getText('Next') + '</button></div>';
			$('#pageNum').html('P.' + (section + 1) + '/4');
			$('#main > form').html(html);
			$(document).scrollTop(0);
			_this.setTrigger();
		}, 'json');
	},
	displayQuestion : function(q) {
		var _this = this;
		var html = '';
		var className = '';
		var classNameWrap = '';
		if (q.q_class != null) {
			className = q.q_class;
			$.each(className.split(" "), function(index, value) {
				classNameWrap += ' ' + value + 'Wrap';
			});

		}
		var domDataHTML = '';
		if (q.q_data != null) {
			var domData = q.q_data;

			$.each(domData.split(","), function(index, value) {
				var datav = value.split("|");
				//var datap = datav[1].split(",");

				domDataHTML += ' data-' + datav[0] + '="' + datav[1] + '" ';
			});

		}
		if (q.q_type == null) {
			return html;
		}
		if (q.q_submit_check != null) {
			var submit_checkClass = ' ' + q.q_submit_check + ' ';
		}

		if (q.q_displayid != "") {
			html = '<div class="form-group MG_QUESTION ' + classNameWrap + '">';
		} else {
			html = '<div class="form-group ' + classNameWrap + ' "' + domDataHTML + '>';
		}

		if (q.q_type == "SECTION") {
			html += '<div class="col-sm-12 MG_TEXT MG_SECTION ' + className + '">' + q.q_displayid + ' ' + q.q_text + '</div>';
		}
		if (q.q_type == "SECTION_PART") {
			html += '<div class="col-sm-12 MG_TEXT MG_SECTION_PART ' + className + '">' + q.q_displayid + ' ' + q.q_text + '</div>';
		}
		if (q.q_type == "TEXT") {
			html += '<label for="' + q.q_formid + '" class="col-sm-12 MG_SELECT" style="display:none;"></label>';
			html += '<div id="' + q.q_formid + '" data-q_id="' + q.q_id + '" class="col-sm-12 MG_TEXT ' + className + '">' + q.q_displayid + ' ' + q.q_text + '</div>';
		}
		if (q.q_type == "TEXT_BOLD") {
			html += '<div data-q_id="' + q.q_id + '" class="col-sm-12 MG_TEXT_BOLD ' + className + '">' + q.q_displayid + ' ' + q.q_text + '</div>';
		}
		if (q.q_type == "INPUT_TEXT") {
			html += '<label for="' + q.q_formid + '" class="col-sm-12 MG_INPUT_TEXT">' + q.q_displayid + ' ' + q.q_text + '</label>';
			html += '<div class="col-sm-12 MG_INPUT_TEXT_BOX">';
			html += '<input type="text" id="' + q.q_formid + '" name="' + q.q_formid + '" class="form-control ' + className + submit_checkClass + '">';
			html += '</div>';
		}
		if (q.q_type == "TEXTAREA") {
			html += '<label for="' + q.q_formid + '" class="col-sm-12 MG_INPUT_TEXT">' + q.q_displayid + ' ' + q.q_text + '</label>';
			html += '<div class="col-sm-12 MG_INPUT_TEXT_BOX">';
			html += '<textarea id="' + q.q_formid + '" name="' + q.q_formid + '" class="form-control ' + className + submit_checkClass + '"></textarea>';
			html += '</div>';
		}
		if (q.q_type == "SELECT") {
			var extraField = new Array();

			html += '<label for="' + q.q_formid + '" class="col-sm-12 MG_SELECT">' + q.q_displayid + ' ' + q.q_text + '</label>';
			html += '<div class="col-sm-12 MG_SELECT_OPTIONS">';
			html += '<select id="' + q.q_formid + '" name="' + q.q_formid + '" class="form-control ' + className + submit_checkClass + '">';
			for (var i = 0; i < q.q_o.length; i++) {
				if (q.q_o[i].o_extra == null) {
					html += '<option value="' + q.q_o[i].o_value + '">' + q.q_o[i].o_text + '</option>';
				} else {
					var temp = q.q_o[i].o_text;
					temp = temp.split(' || ');

					html += '<option value="' + q.q_o[i].o_value + '">' + temp[0] + '</option>';
					extraField.push(new Array(q.q_formid, q.q_o[i].o_value, q.q_o[i].o_extra, temp[1]));
				}
			}
			html += '</select>';
			html += this.displayExtraField(extraField);
			html += '</div>';
		}
		if (q.q_type == "CHECKBOX") {
			var extraField = new Array();
			html += '<label for="' + q.q_formid + '" class="col-sm-12 MG_CHECKBOX">' + q.q_displayid + ' ' + q.q_text + '</label>';
			html += '<div class="col-sm-12 MG_CHECKBOX_OPTIONS  ' + className + submit_checkClass + '">';
			for (var i = 0; i < q.q_o.length; i++) {
				if (q.q_o[i].o_extra == null) {
					if (q.q_o[i].o_value == -1) {
						html += '<div class="MG_TEXT_BOLD ' + className + '">' + q.q_o[i].o_text + '</div>';
					} else {
						html += '<div class="checkbox">';
						html += '<label><input type="checkbox" id="' + q.q_formid + '_' + q.q_o[i].o_value + '" name="' + q.q_formid + '_' + q.q_o[i].o_value + '" value="1">' + q.q_o[i].o_text + '</label>';
						html += '</div>';
					}
				} else {
					var temp = q.q_o[i].o_text;
					temp = temp.split(' || ');
					html += '<div class="checkbox">';
					html += '<label><input type="checkbox" id="' + q.q_formid + '_' + q.q_o[i].o_value + '" name="' + q.q_formid + '_' + q.q_o[i].o_value + '" value="1">' + temp[0] + '</label>';
					html += '</div>';
					extraField.push(new Array(q.q_formid, q.q_o[i].o_value, q.q_o[i].o_extra, temp[1]));
				}
			}
			html += this.displayExtraField(extraField);
			html += '</div>';
		}
		if (q.q_type == "RATING") {
			html += '<label for="q' + q.q_formid + '" class="col-sm-12 MG_SELECT">' + q.q_displayid + ' ' + q.q_text + '</label>';
			html += '<div class="col-sm-12 MG_SELECT_OPTIONS">';
			html += '<select id="' + q.q_formid + '" name="' + q.q_formid + '" class="form-control ' + className + submit_checkClass + '">';
			for (var i = 0; i < q.q_o.length; i++) {
				html += '<option value="' + q.q_o[i].o_value + '">' + q.q_o[i].o_text + '</option>';
			}
			for (var i = 1; i <= 10; i++) {
				html += '<option value="' + i + '">' + i + '</option>';
			}
			html += '</select>';
			html += '</div>';
		}
		if (q.q_type == "SORTING") {
			html += '<label for="' + q.q_formid + '" class="col-sm-12 MG_INPUT_TEXT">' + q.q_displayid + ' ' + q.q_text + '(拉動<span class="glyphicon glyphicon-resize-vertical"></span>排序)</label>';

			html += '<div class="col-sm-12 MG_INPUT_TEXT_BOX">';
			var qMetaList = q.q_meta;
			html += '<ul class="sortable">';
			$.each(qMetaList.split("\n"), function(index, value) {
				if ($.trim(value) !== '') {
					html += '<li class="ui-state-default sorting_item" data-sort="' + index + '" ><span class="handle"><span class="  glyphicon glyphicon-resize-vertical"></span>' + value + '</span></li>';
				}
			});

			html += '</ul>';
			html += '<input type="hidden" id="' + q.q_formid + '" name="' + q.q_formid + '" class="form-control ' + className + submit_checkClass + '" >';
			html += '</div>';
		}

		if (q.q_type == "NEW_CHECKBOX") {
			var extraField = new Array();
			html += '<label for="' + q.q_formid + '" class="col-sm-12 MG_CHECKBOX">' + q.q_displayid + ' ' + q.q_text + '</label>';
			html += '<div  id="' + q.q_formid + '" class="col-sm-12 MG_CHECKBOX_OPTIONS  ' + className + submit_checkClass + '">';
			var qMetaList = q.q_meta;
			$.each(qMetaList.split("\n"), function(index, value) {
				if ($.trim(value) !== '') {
					if (value == '[OTHERS]') {
						html += '<div class="col-sm-12 MG_INPUT_TEXT MG_EXTRA_FIELD">';
						html += '<input type="text" id="' + q.q_formid + '_' + (index) + '_extra" name="' + q.q_formid + '_' + (index) + '_extra" placeholder="' + '請輸入...' + '" class="form-control">';
						html += '</div>';
					} else {
						html += '<div class="checkbox">';
						if (value.indexOf("其他") != -1) {
							html += '<label><input type="checkbox" id="' + q.q_formid + '_' + (index + 1) + '" class="opt_others" name="' + q.q_formid + '_' + (index + 1) + '" value="1">' + value + '</label>';
						} else {
							html += '<label><input type="checkbox" id="' + q.q_formid + '_' + (index + 1) + '" name="' + q.q_formid + '_' + (index + 1) + '" value="1">' + value + '</label>';
						}
						html += '</div>';
					}
				}
			});

			html += '</div>';
		}
		if (q.q_type === "NEW_SELECT") {

			html += '<label for="' + q.q_formid + '" class="col-sm-12 MG_SELECT">' + q.q_displayid + ' ' + q.q_text + '</label>';
			html += '<div class="col-sm-12 MG_SELECT_OPTIONS">';
			html += '<select id="' + q.q_formid + '" name="' + q.q_formid + '" class="form-control ' + className + submit_checkClass + '">';

			var qMetaList = q.q_meta;

			var re = new RegExp('.*\\[' + _this.lang + '\\](.*?)(\\[|$).*');

			$.each(qMetaList.split("\n"), function(index, value) {
				if ($.trim(value) !== '') {
					var value_key = value.split("||");

					value_key[1] = value_key[1].replace(re, '$1');

					html += '<option value="' + value_key[0] + '">' + value_key[1] + '</option>';
				}
			});
			html += '</select>';
			html += '</div>';
		}

		html += '</div>';
		return html;
	},
	displayExtraField : function(extraField) {
		var html = '';

		for (var i = 0; i < extraField.length; i++) {
			if (extraField[i][2] == "INPUT_TEXT") {
				html += '<div class="col-sm-12 MG_INPUT_TEXT MG_EXTRA_FIELD">';
				//html += '<input type="text" id="' + extraField[i][0] + '_' + extraField[i][1] + '_extra" name="' + extraField[i][0] + '_' + extraField[i][1] + '_extra" placeholder="' + extraField[i][3] + '" class="form-control">';
				html += '<input type="text" id="' + extraField[i][0] + '_' + extraField[i][1] + '_extra" name="' + extraField[i][0] + '_' + extraField[i][1] + '_extra"  class="form-control">';
				html += '</div>';
			}
			if (extraField[i][2] == "TEXTAREA") {
				html += '<div class="col-sm-12 MG_INPUT_TEXT_BOX MG_EXTRA_FIELD">';
				html += '<textarea id="' + extraField[i][0] + '_' + extraField[i][1] + '_extra" name="' + extraField[i][0] + '_' + extraField[i][1] + '_extra" placeholder="' + extraField[i][3] + '" class="form-control"></textarea>';
				html += '</div>';
			}
		}
		return html;
	},
	showError : function(e) {
		$(e).parents('.form-group').addClass('has-error');
	},
	showThanksMessage : function() {
		$('#pageNum').html('P. 10/10');
		$('#main > form').html('');
		$('#thanksMessage').html(this.getText('ThanksMessage')).show();
	},
	required_check : function() {
		var _this = this;

		var has_common_error = 0;
		$('#main > form').find('.required').each(function() {
			// if($(this))
			// //console.log($(this).val());

			var tagN = this.tagName;

			if (tagN === 'SELECT' || tagN === 'TEXTAREA') {
				if ($(this).parent().parent('.form-group').is(":visible")) {
					if ($(this).val() == '' || $(this).val() == '-1') {
						_this.showError('#' + this.id);
						has_common_error = 1;
					}
				}
			}

			if (tagN === 'INPUT' && $(this).attr('type') === 'text') {
				if ($(this).parent().parent('.form-group').is(":visible")) {
					if ($(this).val() === '') {
						_this.showError('#' + this.id);
						has_common_error = 1;
					}
				}
			}

			if (tagN === 'INPUT' && $(this).attr('type') === 'hidden') {
				if ($(this).val() === '') {
					_this.showError('#' + this.id);
					has_common_error = 1;

				}
			}

			if (tagN === 'DIV' && $(this).hasClass('MG_CHECKBOX_OPTIONS')) {
				if ($(this).parent('.form-group').is(":visible")) {
					if ($(this).find("input[type='checkbox']:checked").length == 0) {
						$(this).parents('.form-group').addClass('has-error');
						has_common_error = 1;
					}
				}
			}

		});

		/**Specific Check **/

		if ($("input[id^='p1_2_']").length > 0 && $("input[id^='p1_2_']:checked").length != 3) {
			$("#p1_2").addClass('has-error');
			has_common_error = 1;
		} else {
			$("#p1_2").removeClass('has-error');
		}

		return has_common_error;
	},
	saveQuestions : function() {
		var _this = this;
		$('.form-group').removeClass('has-error');
		var required_check = _this.required_check();
		//var required_check = 0;
		if (required_check == 1) {
			alert('請輸入未完成的紅色部份');
			return false;
		}
		$('#q_yos').val($('#yosSelectY').val() + '/' + $('#yosSelectM').val());
		$('#q_yosPosition').val($('#yosPositionSelectY').val() + '/' + $('#yosPositionSelectM').val());

		var answer = $('#main > form').serialize();
		//console.log('answer');
		//console.log(answer);

		var valid = _this.checkFields(_this.section, $('#main > form').serializeObject());

		if (valid && !_this.checking) {
			_this.checking = true;
			$('#overlay').show();
			$.post('api/saveSurvey.php', {
				"lang" : _this.lang,
				"section" : _this.section,
				"a_id" : _this.a_id,
				"answer" : answer
			}, function(result) {
				_this.checking = false;
				$('#overlay').hide();
				if (_this.section == 0) {
					_this.a_id = result;

					_this.getQuestions(1);
				} else {
					if (_this.section == 3) {
						_this.finished = true;
						_this.showThanksMessage();
					} else {
						_this.getQuestions(_this.section + 1);
					}
				}
			}, 'json');
		}
	},
	setTrigger : function() {

		//END PAGE
		if (this.section == 7) {
			$('.MG_BUTTON_PANEL').hide();
		}

		$(".sortable").sortable({
			placeholder : "ui-state-highlight",

			update : function(e, ui) {
				var anw = '';

				$(this).find('li').each(function() {
					anw += $(this).data('sort');
				});
				$(this).parent().children('input').val(anw);
			}
		});

		$("input[id^='p1_2_']").click(function() {
			if ($("input[id^='p1_2_']:checked").length > 3) {
				alert("最多只能選3個");
				$(this).prop('checked', false);
				if ($(this).attr('id') == 'p1_2_10') {
					$("#p1_2_10_extra").parent().addClass('MG_EXTRA_FIELD');
				}
			}
		});
		$("#p1_4").change(function() {
			if ($(this).val() == 2) {
				$("label[for='p1_4_t']").parent(".form-group").nextAll(".form-group").hide().removeClass('has-error').children('div').children('input').removeClass('required');
				$("#p1_5").val(-1);
				$("#p1_5_r").val('');
			} else {
				$("label[for='p1_4_t']").parent(".form-group").nextAll(".form-group").show().children('div').children('input').addClass('required');
				$("#p1_4_r").val('');
				if($("#p1_5").val() != 2) {
					$("#p1_5_r").parent().parent(".form-group").hide();
					$("#p1_5_t").parent(".form-group").hide();
				}
				if($("#p1_8_2").val()!=1){
					$("#p1_8_2_r").parent(".form-group").hide();
				}
				if($("#p1_8_3").val()!=1){
					$("#p1_8_3_r").parent().parent(".form-group").hide();
				}
				if($("#p1_12_2").val()!=4){
					$("#p1_12_2_r").parent().parent(".form-group").hide();
				}
				if($("#p1_14_1").val()!=2){
					$("#p1_14_1_r").parent(".form-group").hide();
				}
				if($("#p1_14_1").val()!=1){
					$("#p1_14_2").parent(".form-group").hide();
					$("#p1_14_2_1").parent().parent(".form-group").hide();
					$("#p1_14_2_2").parent().parent(".form-group").hide();
					$("#p1_14_2_3").parent().parent(".form-group").hide();
				} 				
			}
		});
		$("#p1_5").change(function() {
			if ($(this).val() == 2) {
				$("label[for='p1_5_t']").parent(".form-group").nextAll(".form-group").hide();
			} else {
				$("label[for='p1_5_t']").parent(".form-group").nextAll(".form-group").show();
				$("#p1_5_r").val('');
				$("#p1_5_t").parent(".form-group").hide();
				if($("#p1_8_2").val()!=1){
					$("#p1_8_2_r").parent(".form-group").hide();
				}
				if($("#p1_8_3").val()!=1){
					$("#p1_8_3_r").parent().parent(".form-group").hide();
				}
				if($("#p1_12_2").val()!=4){
					$("#p1_12_2_r").parent().parent(".form-group").hide();
				}
				if($("#p1_14_1").val()!=2){
					$("#p1_14_1_r").parent(".form-group").hide();
				}
				if($("#p1_14_1").val()!=1){
					$("#p1_14_2").parent(".form-group").hide();
					$("#p1_14_2_1").parent().parent(".form-group").hide();
					$("#p1_14_2_2").parent().parent(".form-group").hide();
					$("#p1_14_2_3").parent().parent(".form-group").hide();
				} 	
				
			}
		});

		$("#p1_8_3").change(function() {
			if ($(this).val() != 1) {
				$("#p1_8_3_r").val('');
			}
		});
		$("#p1_12_2").change(function() {
			if ($(this).val() != 4) {
				$("#p1_12_2_r").val('');
			}
		});

		$("#p1_14_1").change(function() {
			if ($(this).val() == 2) {
				$("label[for^='p1_14_2']").parent('.form-group').hide();
				$("label[for^='p1_14_3']").parent('.form-group').hide();
				$("input[id^='p1_14_2']").val('');
				$("textarea[id^='p1_14_3']").val('');
			} else if ($(this).val() == 1) {
				$("label[for^='p1_14_2']").parent('.form-group').show();
				$("label[for^='p1_14_3']").parent('.form-group').show();
			} else {
				$("label[for^='p1_14_2']").parent('.form-group').hide();
				$("label[for^='p1_14_3']").parent('.form-group').hide();
			}
		});

		$("#p2_25_2").change(function() {
			if ($(this).val() != 2) {
				$("#p2_25_2_r").val('');
			}
		});

		$("#p2_29").change(function() {
			if ($(this).val() != 2) {
				$("#p2_29_r").val('');
			}
		});

		$('.under7').change(function() {
			var value = $(this).val();
			var wrap = $(this).parents('.under7Wrap');
			var trigger_id = wrap.data('show');

			if (value < 7) {
				$('#' + trigger_id).parents('.form-group').show();
				$('#' + trigger_id).addClass('required');
			} else {
				$('#' + trigger_id).parents('.form-group').hide();
				$('#' + trigger_id).removeClass('required');
			}
		});

		$('.equal1').change(function() {
			var value = $(this).val();
			var wrap = $(this).parents('.equal1Wrap');
			var trigger_id = wrap.data('show');
			if (value == 1) {
				$('#' + trigger_id).parents('.form-group').show();
				$('#' + trigger_id).addClass('required');
			} else {
				$('#' + trigger_id).parents('.form-group').hide();
				$('#' + trigger_id).removeClass('required');
			}
		});

		$('.equal2').change(function() {
			var value = $(this).val();
			var wrap = $(this).parents('.equal2Wrap');
			var trigger_id = wrap.data('show');
			var triggerGroup = '';
			if (trigger_id.indexOf("*") >= 0) {
				var temp = trigger_id.split("*");
				for (var i = 0; i < temp.length; i++) {
					triggerGroup += "#" + temp[i];
					if (i < temp.length - 1)
						triggerGroup += ",";
				}
			} else {
				triggerGroup = "#" + trigger_id;
			}
			if (value == 2) {
				$(triggerGroup).parents('.form-group').show();
				$(triggerGroup).addClass('required');
			} else {
				$(triggerGroup).parents('.form-group').hide();
				$(triggerGroup).removeClass('required');
			}
		});

		$('.equal4').change(function() {
			var value = $(this).val();
			var wrap = $(this).parents('.equal4Wrap');
			var trigger_id = wrap.data('show');
			if (value == 4) {
				$('#' + trigger_id).parents('.form-group').show();
				$('#' + trigger_id).addClass('required');
			} else {
				$('#' + trigger_id).parents('.form-group').hide();
				$('#' + trigger_id).removeClass('required');
			}
		});

		$('.equal17').change(function() {
			var value = $(this).val();
			var wrap = $(this).parents('.equal17Wrap');
			var trigger_id = wrap.data('show');
			if (value == 17) {
				$('#' + trigger_id).parents('.form-group').show();
				$('#' + trigger_id).addClass('required');
			} else {
				$('#' + trigger_id).parents('.form-group').hide();
				$('#' + trigger_id).removeClass('required');
			}
		});

		$('.opt_others').change(function() {
			var checked = $(this).is(":checked");
			var name = $(this).attr('id');
			if (checked) {
				$("#" + name + "_extra").parent().removeClass('MG_EXTRA_FIELD');
				$("#" + name + "_extra").addClass('required');
			} else {
				$("#" + name + "_extra").parent().addClass('MG_EXTRA_FIELD');
				$("#" + name + "_extra").removeClass('required');
			}
		});

		$('.qLeave').change(function() {
			var value = $(this).val();
			if (value === '1') {
				$('.qLeaveZoneWrap').show();
				$('.qLeaveZone').addClass('required');
			} else {
				$('.qLeaveZoneWrap').hide();
				$('.qLeaveZone').removeClass('required');
			}
		});
		$("#p3_7").change(function() {
			var value = $(this).val();
			if (value != '2') {
				$("#p3_7_r").val('');
			}
		});

		$("#p3_8").change(function() {
			var value = $(this).val();
			if (value != '2') {
				$("#p3_8_r").val('');
			}
		});

		$("#p3_10").change(function() {
			var value = $(this).val();
			if (value != '2') {
				$("#p3_10_r").val('');
			}
		});

		$("#p1_8_1_6").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p1_8_1_6_extra").val('');
			}
		});

		$("#p1_8_2_r_5").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p1_8_2_r_5_extra").val('');
			}
		});

		$("#p1_10_1_6").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p1_10_1_6_extra").val('');
			}
		});

		$("#p1_10_2_6").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p1_10_2_6_extra").val('');
			}
		});

		$("#p1_1_14").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p1_1_14_extra").val('');
			}
		});

		$("#p1_2_10").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p1_2_10_extra").val('');
			}
		});

		$('#p1_3_1b_12').click(function() {
			var checked = $(this).is(":checked");
			if (checked) {
				$('#p1_3_1b_12_extra').parent().removeClass('MG_EXTRA_FIELD');
				$('#p1_3_1b_12_extra').addClass('required');
			} else {
				$('#p1_3_1b_12_extra').parent().addClass('MG_EXTRA_FIELD');
				$('#p1_3_1b_12_extra').removeClass('required');
			}
		});

		$("#p2_18_7").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p2_18_7_extra").val('');
			}
		});

		$("#p2_19_7").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p2_19_7_extra").val('');
			}
		});

		$("#p3_6_26").click(function() {
			var checked = $(this).is(":checked");
			if (checked) {
				$("#p3_6_26_r").parent().show();
				$("#p3_6_26_r").addClass('required');
			} else {
				$("#p3_6_26_r").parent().hide();
				$("#p3_6_26_r").removeClass('required');
				$("#p3_6_26_r").val('');
			}
		});
		$("#p3_6_38").click(function() {
			var checked = $(this).is(":checked");
			if (checked) {
				$("#p3_6_38_r").parent().show();
				$("#p3_6_38_r").addClass('required');
			} else {
				$("#p3_6_38_r").parent().hide();
				$("#p3_6_38_r").removeClass('required');
				$("#p3_6_38_r").val('');
			}
		});
		$("#p3_6_39").click(function() {
			var checked = $(this).is(":checked");
			if (checked) {
				$("#p3_6_39_r").parent().show();
				$("#p3_6_39_r").addClass('required');
			} else {
				$("#p3_6_39_r").parent().hide();
				$("#p3_6_39_r").removeClass('required');
				$("#p3_6_39_r").val('');
			}
		});
		$("#p3_6_40").click(function() {
			var checked = $(this).is(":checked");
			if (checked) {
				$("#p3_6_40_r").parent().show();
				$("#p3_6_40_r").addClass('required');
			} else {
				$("#p3_6_40_r").parent().hide();
				$("#p3_6_40_r").removeClass('required');
				$("#p3_6_40_r").val('');
			}
		});
		$("#p3_9_6").click(function() {
			var checked = $(this).is(":checked");
			if (!checked) {
				$("#p3_9_6_extra").val('');
			}
		});
		$("#p3_9 .checkbox label input[type='checkbox']").click(function(){
			var name = $(this).attr('id');
			var checked = $(this).is(":checked");
			if (name != 'p3_9_5') {
				if (checked) {
					$("#p3_9_5").prop("checked", false);
				}
			} else {
				if (checked) {
					$("#p3_9_5").parent().parent().siblings('.checkbox').children('label').children('input[type="checkbox"]').prop("checked", false);
					$("#p3_9_6_extra").val('').parent().addClass("MG_EXTRA_FIELD");
				}
			}
		});

		$("#p3_11 .checkbox label input[type='checkbox']").off("click").on("click", function() {
			var name = $(this).attr('id');
			var checked = $(this).is(":checked");
			if (name != 'p3_11_4') {
				if (checked) {
					$("#p3_11_4").prop("checked", false);
				}
			} else {
				if (checked) {
					$("#p3_11_4").parent().parent().siblings('.checkbox').children('label').children('input[type="checkbox"]').prop("checked", false);
				}
			}
		});

		$('body').keypress(function(event) {
			if (event.keyCode === 13 && event.target.tagName == 'input') {
				$("input").blur();
				event.preventDefault();
				return false;
			}
		});

	},
	checkFields : function(section, fields) {

		var valid = true;
		//console.log("section : " + section + ", fields : " + fields);
		if (section == 0) {
			if (!$.trim($('#custid').val())) {
				valid = false;
				$('#custidError').html('請輸入編號.');
				$('#custidError').show();
			} else {
				if (!/^[A-Za-z]{1}[0-9]{8}$/.test($('#custid').val())) {
					valid = false;
					$('#custidError').html('請輸入正確的編號.');
					$('#custidError').show();
				} else {
					$('#custidError').hide();
				}
			}

			if (!$.trim($('#engname').val())) {
				valid = false;
				$('#engnameError').html('請輸入英文名字.');
				$('#engnameError').show();
			} else {
				if (!/^[A-Za-z ]{3,}$/.test($('#engname').val())) {
					valid = false;
					$('#engnameError').html('英文名字格式錯誤.請重新輸入.');
					$('#engnameError').show();
				} else {
					$('#engnameError').hide();
				}
			}

			if (!$.trim($('#mobile').val())) {
				valid = false;
				$('#phoneError').html('請輸入手提電話.');
				$('#phoneError').show();
			} else {
				if (!/^[0-9]{8}$/.test($('#mobile').val())) {
					valid = false;
					$('#phoneError').html('手提電話格式錯誤.請重新輸入.');
					$('#phoneError').show();
				} else {
					if ($('#mobile').val().substr(0, 1) == "0" || $('#mobile').val().substr(0, 1) == "1" || $('#mobile').val().substr(0, 1) == "4") {
						$('#phoneError').html('手提電話格式錯誤.請重新輸入.');
						$('#phoneError').show();
					} else {
						$('#phoneError').hide();
					}
				}
			}

			if ($('input[name="location"]:checked').length == 0) {
				valid = false;
				$('#locationError').show();
			} else {
				$('#locationError').hide();
			}
		}
		////console.log("Valid : "+valid);
		return valid;

	}
};

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push($.trim(this.value) || '');
		} else {
			o[this.name] = $.trim(this.value) || '';
		}
	});
	return o;
};
