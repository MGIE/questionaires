<?php
require_once 'api/mglib/Mobile_Detect.php';
$detect=new Mobile_Detect;
if($detect->isMobile()){
    $device="M";
}else{
    $device="P";
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap/bootstrap-theme.min.css">
		<!--[if IE 8]><link rel="stylesheet" href="css/bootstrap-ie8buttonfix.css"><![endif]-->
		<link rel="stylesheet" href="css/mg-form.css">
	</head>
	<style>
	    .MG_SECTION{
	        color:#000;
	        background:#FFF;
	        border-left: none;
	    }
	    .listImgWrap{
	        width:100%;
	    }
	    .listImgWrap:after{
	        clear:both;
            content: ' ';
            display:table;    
	    }
	    .listImg{
	        float:left;	        
	    }
	    .listImg img{
	       max-height: 200px;
	    }
	    .listNameArea{
	        width:100%;
	    }
	    .listNameArea:after{
	        clear:both;
	        content: ' ';
	        display:table;	        	        
	    }
	    .list_shopName_item{
	        float:left;
	        width:50%;
	    }	   
	    /***Landing Form***/  
	    .listArea{
	          width:100%;
	    }
	    .listArea:after{
            clear:both;
            content: ' ';
            display:table;                      
        }
        .list_item{
            float:left;
            width:50%;
        }   
         .list_item:first-child{
            float:left;
            width:25%;
        }      
       
	  @media all and (max-width: 768px) {
	      .list_shopName_item{
            float:none;
            width:100%;
        }        
        .listImg{
            float:none;
            margin-top:5px;
            width:100%;
        }
        .list_item, .list_item:first-child{
           float:none;
            width:100%;
        }      
	  }
	</style>
	<body>

		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#langPanel">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.php">Home</a>
					</div>
                                    <!--
					<div class="collapse navbar-collapse" id="langPanel">
						<p id="pageNum" class="navbar-text"></p>
						<ul class="nav navbar-nav navbar-right">
							<li class="active">
								<a href="#en" data-lang="en">English</a>
							</li>
							<li>
								<a href="#tc" data-lang="tc">繁體中文</a>
							</li>
							<li>
								<a href="#sc" data-lang="sc">简体中文</a>
							</li>
							<li>
								<a href="#th" data-lang="th">ไทย</a>
							</li>
							<li>
								<a href="#kr" data-lang="kr">한국어</a>
							</li>
							<li>
								<a href="#jp" data-lang="jp">日本語</a>
							</li>
						</ul>
					</div>
                                    -->
				</div>
			</nav>

			<div class="row">
				<div id="surveyTitle" class="col-sm-12">
                  活動已完畢，多謝各位支持!
				</div>
			</div>
			<div id="main">
				<form class="form-horizontal clearfix" role="form">

				</form>
				<div id="thanksMessage" class="col-sm-12 alert alert-success">

				</div>

			</div>
		</div>
		<div id="overlay">
			<table>
				<tr>
					<td><img src="loading.gif"></td>
				</tr>
			</table>
		</div>
		<?php /*
            <script src="js/jquery-1.10.2.min.js"></script>
                <script src="http://code.jquery.com/jquery-migrate-1.1.1.js"></script>
           
           <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>     
               <script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
		<script src="js/jquery.placeholder.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
		<![endif]-->
		<script src="js/kiehlSurveyLanguage.js"></script>
		<script src="js/kiehlSurveyController.js"></script>
         
               <script src="js/jquery.ui.touch-punch.min.js"></script>
		<script>

			$(document).ready(function() {
				var hash = $(location).attr('hash').replace('#', '');
				if (hash != "tc" && hash != "sc" && hash != "th" && hash != " kr" && hash != "jp") {
					hash = "";
				}
				var lang = (hash == "") ? "tc" : hash;
				kiehlSurveyController.initial(lang,'<?php echo $device;?>');
                                /*for debug jump page*/
                                /*var page = $(location).attr('hash').replace('#', '');
                                var patt = /^\d+$/;
                             
                                if(patt.test(page)){
                                     kiehlSurveyController.getQuestions(page);
                                }else{
                                    kiehlSurveyController.getQuestions(0);
                                }
                        
				
				$('#langPanel li a').each(function() {
					$(this).click(function() {
						$('#langPanel li').removeClass('active');
						$(this).parent().addClass('active');
						kiehlSurveyController.changeLanguage($(this).data('lang'));
					})
				});
			});
                        
                    
		</script>
		*/?>
	</body>
</html>