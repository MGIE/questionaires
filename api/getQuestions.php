<?php
header("Content-Type:text/javascript; charset=utf-8");
require_once ("mglib/AppUtility.php");
require_once ("mglib/KiehlSurveyManager.php");

$__u = new AppUtility();
$km = new KiehlSurveyManager();

$lang = $__u -> getRequest("lang");
$section = $__u -> getRequest("section");
$questions = $km -> getQuestions($section);

//$formid_array = array();
//foreach($questions AS $k =>$v){
//    if($v['q_formid']){
//        $formid_array[$v['q_formid']] = $v['q_text_tc'];
//        if($v['q_type']=='NEW_CHECKBOX'){
//                $metas = explode("\n", $v['q_mate']);
//                foreach($metas AS $k1 =>$v1){
//                    $formid_array[$v['q_formid'].'_'.($k1+1)]=$v1;
//                }
//        }
//    }
//    
//}


$result = array();
if ($questions != null && sizeof($questions) > 0) {
    for ($i = 0; $i < sizeof($questions); $i++) {

        if ($questions[$i]['q_type'] == "SELECT") {
            $question_options = $km -> getQuestionOptions($questions[$i]["q_id"]);
            if ($question_options != null && sizeof($question_options) > 0) {
                for ($j = 0; $j < sizeof($question_options); $j++) {
                    $question_options[$j]['o_text'] = $question_options[$j]['o_text_' . $lang];
                }
                $questions[$i]['q_o'] = $question_options;
            }
        }

        if ($questions[$i]['q_type'] == "CHECKBOX") {
            $question_options = $km -> getQuestionOptions($questions[$i]["q_id"]);
            if ($question_options != null && sizeof($question_options) > 0) {
                for ($j = 0; $j < sizeof($question_options); $j++) {
                    $question_options[$j]['o_text'] = $question_options[$j]['o_text_' . $lang];
                }
                $questions[$i]['q_o'] = $question_options;
            }
        }

        if ($questions[$i]['q_type'] == "RATING") {
            $question_options = $km -> getQuestionOptions($questions[$i]["q_id"]);
            if ($question_options != null && sizeof($question_options) > 0) {
                for ($j = 0; $j < sizeof($question_options); $j++) {
                    $question_options[$j]['o_text'] = $question_options[$j]['o_text_' . $lang];
                }
                $questions[$i]['q_o'] = $question_options;
            }
        }

        $questions[$i]['q_displayid'] = $questions[$i]['q_displayid'] == null ? "" : $questions[$i]['q_displayid'] . ".";
        $questions[$i]['q_text'] = $questions[$i]['q_text_' . $lang] == null ? "" : $questions[$i]['q_text_' . $lang];
        array_push($result, $questions[$i]);
    }
}

echo json_encode($result);
exit();
?>