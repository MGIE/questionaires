<?php
class AppUtility {

    public function AppUtility() {

    }

    public function getRequest($req) {
        try {
            if (array_key_exists($req, $_REQUEST)) {
                return trim($_REQUEST[$req]);
            } else {
                return "";
            }
        } catch (Exception $e) {
            echo 'Caught exception at $req: ', $e -> getMessage(), "\n";
        }
        return "";
    }

    public function getProtocol() {
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https:" : "http:";
        return $protocol;
    }

    public function getIP() {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            $ip = $_SERVER['HTTP_FORWARDED'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = 'UNKNOWN';
        }

        return $ip;
    }

    public function getUserAgent() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    public function debugLog($part, $data) {
        $message = "[" . date("Y-m-d H:i:s") . " / " . $part . "] " . $data . "\n";
        error_log($message, 3, "../log/debugLog");
        return;
    }

}
?>