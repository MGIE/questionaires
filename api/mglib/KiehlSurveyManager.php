<?php
require_once ("config.php");
require_once ("Connection.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');

class KiehlSurveyManager {
    private $db;
    private $createTime;

    public function KiehlSurveyManager() {
        $this -> db = new Connection();
        $this -> createTime = date("Y-m-d H:i:s");
    }

    public function getQuestions($section) {
        $section = $this -> db -> escape($section);

        if ($section == -1) {
            $query = "SELECT * FROM `tbl_question` ORDER BY `q_order` DESC,`q_id` ASC";
        } else {
            $query = "SELECT * FROM `tbl_question` WHERE `q_section` = '" . $section . "' ORDER BY  `q_order` DESC,`q_id` ASC";
        }
        $result = $this -> db -> master_query($query);
        return $result;
    }

    public function getQuestionOptions($qid) {
        $qid = $this -> db -> escape($qid);

        $query = "SELECT * FROM `tbl_question_option` WHERE `q_id` = '" . $qid . "' ORDER BY `o_priority` DESC, `o_id` ASC";
        $result = $this -> db -> master_query($query);
        return $result;
    }

    public function createSurvey($lang, $ip, $userAgent, $answer) {
        $lang = $this -> db -> escape($lang);
        $ip = $this -> db -> escape($ip);
        $userAgent = $this -> db -> escape($userAgent);
        $answer = explode('&', $answer);


        $col = array('`lang`', '`createTime`', '`ip`', '`userAgent`', '`custid`', '`engname`', '`mobile`', '`location`');
        $val = array('"' . $lang . '"', '"' . date("Y/m/d H:i:s") . '"', '"' . $ip . '"', '"' . $userAgent . '"');
         for ($i = 0; $i < count($answer); $i++) {
              $answer[$i] = explode('=', $answer[$i]);
              array_push($val, '"'.$this -> db -> escape(urldecode($answer[$i][1])).'"');
         }         
        $col = implode(",", $col);
        $val = implode(",", $val);
        $sql = "INSERT INTO `tbl_answer`(" . $col . ") VALUES(" . $val . ");";
        $result = $this -> db -> master_insert($sql);

        return $result;
    }

    public function saveSurvey($a_id, $answer) {
        $a_id = $this -> db -> escape($a_id);
        $answer = explode('&', $answer);
        $sql = "";
        
        for ($i = 0; $i < count($answer); $i++) {
            $answer[$i] = explode('=', $answer[$i]);

            $key = isset($answer[$i][0]) ? $this -> db -> escape($answer[$i][0]) : NULL;
            $value = isset($answer[$i][1]) ? $this -> db -> escape(urldecode($answer[$i][1])) : NULL;
            $getkey[]=$key;//joe
            $sql .= '`' . $key . '`="' . $value . '", ';
        }
  
        $sql = "UPDATE `tbl_answer` SET " . $sql . "`lastUpdate`='" . date("Y/m/d H:i:s") . "' WHERE `a_id`='" . $a_id . "';";
        $result = $this -> db -> master_update($sql);

        return $result;
    }

    public function getSurveryAnswer() {
        $result = array();

        $query = "SELECT * FROM `tbl_answer` ORDER BY `a_id` ASC";
        $result = $this -> db -> master_query($query);

        return $result;
    }

}
?>