<?php
class Connection {
    private $dbh;

    public function Connection() {
        $this -> getHander();
    }

    private function getHander() {
        if (!$this -> dbh) {
            $this -> dbh = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die("Unable to connect to MySQL");
            if (!($selected = mysql_select_db(DB_NAME, $this -> dbh) or die("Could not select db"))) {
                $this -> dbh = "";
            }
        }
    }

    private function debugLog($num, $sql) {
        if (!$this -> dbh) {
            return false;
        }

        $log = "[" . date("Y-m-d H:i:s") . "] (" . $num . ") " . $sql . "\n";
        error_log($log, 3, "../log/dbLog");
    }

    public function escape($str) {
        if (!$this -> dbh) {
            return false;
        }

        return trim(mysql_real_escape_string($str, $this -> dbh));
    }

    public function master_insert($sql) {
        if (!$this -> dbh) {
            return false;
        }

        mysql_query("SET CHARACTER SET UTF8;");
        mysql_query($sql);
        $id = mysql_insert_id();
        $this -> debugLog($id, $sql);

        return $id;
    }

    public function master_update($sql) {
        if (!$this -> dbh) {
            return false;
        }

        mysql_query("SET CHARACTER SET UTF8;");
        mysql_query($sql);
        $rows = mysql_affected_rows();
        $this -> debugLog($rows, $sql);

        return $rows;
    }

    public function master_query($sql) {
        if (!$this -> dbh) {
            return false;
        }

        mysql_query("SET CHARACTER SET UTF8;");
        $result = mysql_query($sql);
        $arr = array();

        if (!mysql_error()) {
            while ($row = mysql_fetch_assoc($result)) {
                array_push($arr, $row);
            }
            mysql_free_result($result);
        } else {
            echo mysql_error();
        }

        return $arr;
    }

}
?>