<?php
header("Content-Type:text/javascript; charset=utf-8");
require_once ("mglib/AppUtility.php");
require_once ("mglib/KiehlSurveyManager.php");

$__u = new AppUtility();
$km = new KiehlSurveyManager();

$lang = $__u -> getRequest("lang");
$section = $__u -> getRequest("section");
$a_id = $__u -> getRequest("a_id");
$answer = $__u -> getRequest("answer");
$ip = $__u -> getIP();
$userAgent = $__u -> getUserAgent();
//echo $lang;
//genTableCol($answer);

if ($section == 0) {
    // Create a record and return a_id
    $result = $km -> createSurvey($lang, $ip, $userAgent, $answer);
} else {
    // Update record by a_id
    $result = $km -> saveSurvey($a_id, $answer);
}

echo json_encode($result);
exit();


function genTableCol($answer) {
        $getkey =array();//joe
        $answer = explode('&', $answer);
        $answer= array_reverse($answer);
                for ($i = 0; $i < count($answer); $i++) {
            $answer[$i] = explode('=', $answer[$i]);

            $key = isset($answer[$i][0]) ? $answer[$i][0] : NULL;
            $value = isset($answer[$i][1]) ? urldecode($answer[$i][1]) : NULL;
            $getkey[$key]=$value;//joe

        }
        

  
        $sql = '';
        foreach($getkey AS $k => $v){
            $sql .= $sql?',':'';
            $type = $v=='tt'?'TEXT':'VARCHAR(45)';
            $sql .="ADD COLUMN `{$k}` {$type} NULL AFTER `a_id`".PHP_EOL;
        }
       echo 'ALTER TABLE `loreal_ufi_survey`.`tbl_answer` '.PHP_EOL.$sql;
        exit();
    }
?>