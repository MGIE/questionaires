<?php

header("Content-Type:text/javascript; charset=utf-8");
require_once ("mglib/AppUtility.php");
require_once ("mglib/KiehlSurveyManager.php");

$__u = new AppUtility();
$km = new KiehlSurveyManager();

$data = $km->getSurveryAnswer();

$killcol = array('ip', 'userAgent');

$questions = $km->getQuestions(-1);

/*$formid_array = array('p_1_5_3ao' => '最應該投放時間(其他)',
    'p_1_5_3a' => '最應該投放時間排列'
);*/
$value_array = array();

//$a = array(array('data' => '-1', 'tc' => '-- 請選擇 --'), array('data' => '1', 'tc' => '有'), array('data' => '1', 'tc' => '沒有'));
//
//echo json_encode($a);
//exit();

foreach ($questions AS $k => $v) {
    if ($v['q_formid']) {
        $formid_array[$v['q_formid']] = $v['q_text_tc'];
        if ($v['q_type'] == 'NEW_CHECKBOX') {
            $metas = explode("\n", $v['q_meta']);
            foreach ($metas AS $k1 => $v1) {
                $formid_array[$v['q_formid'] . '_' . ($k1 + 1)] = $v1;
            }
        }
        if( $v['q_type'] == 'CHECKBOX'){
            $opt = $km->getQuestionOptions($v['q_id']);
            $ary = array();
            foreach ($opt AS $k1 => $v1) {
               $formid_array[$v['q_formid'] . '_' . $v1['o_value']] = $v1['o_text_tc'];
            }
            
        }        
        
         if ($v['q_type'] == 'NEW_SELECT') {
             $metas = explode("\n", $v['q_meta']);
             $ary =array();
             
             foreach ($metas AS $k1 => $v1) {
                 if(preg_match('/\|\|/', $v1)){
                
                 $row = explode("||", $v1);
          
                 $name = preg_replace('{.*?\[tc\](.*?)(\[.*|$)}',"$1",$row[1]);
                 $ary[$row[0]]=$name;
                 }
            }
            $value_array[$v['q_formid']]=$ary;
         }

        if ($v['q_type'] == 'SELECT') {
            $opt = $km->getQuestionOptions($v['q_id']);
           // print_r($opt);
            $ary = array();
            foreach ($opt AS $k1 => $v1) {
                $ary[$v1['o_value']] = $v1['o_text_tc'];
            }
            $value_array[$v['q_formid']] = $ary;
        }
    }
}
//print_r($value_array);
//exit();

foreach ($data as $k => $v) {
    foreach ($killcol as $v1) {
        unset($data[$k][$v1]);
    }
    foreach ($v as $colname => $v1) {
        if (isset($value_array[$colname])) {
            $data[$k][$colname] = isset($value_array[$colname][$v1])?$value_array[$colname][$v1]:$v1;
            
        }
    }
}

foreach ($data as $k => $v) {

    if ($k == 0) {
        $result[0] = array();
        $result[1] = array();
        foreach ($v as $k1 => $v1) {
            $result[0][] = $k1;
            $result[1][] = isset($formid_array[$k1]) ? $formid_array[$k1] : $k1;
        }
    }
    $result[] = $v;
}

//if ($data != null && sizeof($data) > 0) {
//    for ($i = 0; $i < sizeof($data); $i++) {
//        $temp = array();
//        $data[$i]['q_yos'] = str_replace('/', 'y', $data[$i]['q_yos']) . "m";
//        $data[$i]['q_yosPosition'] = str_replace('/', 'y', $data[$i]['q_yosPosition']) . "m";
//        for ($j = 0; $j < sizeof($col); $j++) {
//            $field = $col[$j];
//            $temp[$field] = $data[$i][$field];
//        }
//        array_push($result, $temp);
//    }
//}

$file = date("Y-m-d-H-i-s") . ".csv";
$fp = fopen("../csv/" . $file, 'w');
fwrite($fp, "\xEF\xBB\xBF");
foreach ($result as $fields) {
    fputcsv($fp, $fields);
}
fclose($fp);

echo json_encode($file);
exit();
?>